# document any design assumptions

* Users will need to access the page from various devices including  
mobile phones.
* Accessibilty is important.
* For the purposes of this exercise we will keep things simple so we don't have  
to pass around too much data or use Vuex.

* MVP is as stated on the google doc. Add to this but only within the confines  
of SEO, accessibility and, if possible, optimized and customisable.
