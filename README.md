# productinate

# dotenv
Copy .env.example to .env and adjust the values where necessary.

## Project setup
We will be using node 14.16.0 for this project.
```
$ npm install
```
## Json-Server
Install json-server from npm (using )
```
$ npm i -g json-server
```
and get it running and watching the json db file
```
$ json-server --watch /path/to/database.json
```

## Tailwind errors?
There shouldn't be but if you get postcss errors then you'll need to install compatibility versions.
```
$ npx tailwindcss init --full -p
$ npm uninstall tailwindcss postcss autoprefixer
$ npm install -D tailwindcss@npm:@tailwindcss/postcss7-compat @tailwindcss/postcss7-compat postcss@^7 autoprefixer@^9
```
once the PostCSS tailwind plugin's has been updated (eta unknown) you can reverse the above
```
$ npm uninstall tailwindcss @tailwindcss/postcss7-compat
$ npm install -D tailwindcss@latest postcss@latest autoprefixer@latest
```

## Compiles and hot-reloads for development
```
$ npm run serve
```

## Future things I will do
* Add dark mode and/or 'select a colour scheme' option
* Add icons for a nicer UI where they make sense
* Turn the nav into a component
* Use 5*star icons for 'stars', and work out how to render the colour  
in relation to the star rating (e.g. 3.4 would colour in 68% of the 5 stars)
* Add the ability to fully CRUD
